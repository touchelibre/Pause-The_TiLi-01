[Site Web du Projet](http://touchelibre.fr)

# Pause Thé : TiLi-01

![Logo Projet](01_Système/05_Logo/Logo_ToucheLibre_V3.png)


## Présentation du Projet

![3D de l’objet 0](02_Mécanique/03_Plan_3D/Pause_Thé_0.png)
![3D de l’objet 1](02_Mécanique/03_Plan_3D/Pause_Thé_1.png)
![3D de l’objet 2](02_Mécanique/03_Plan_3D/Pause_Thé_2.png)

Le projet TiLi-01 a pour but de proposer un objet ludique pour la pause thé. L’idée de favoriser la création d’un petit rituel personnel en donnant à l’objet de mulitiple façon de positionner les éléments et de s’amuser.

Une électronique pourrait être ajouter pour : mesurer la température du thé ; mesurer le temps d’infusion.


## Avancement

Voici le reste à faire et l’intention final du projet. L’avancement est donné entre accolade {x %}.

* [x] {100%} Réalisation d’un premier prototype pour éprouver le concept.
* [x] {100%} Spécification générale
* [x] {100%} Dessin du boitier en 3D
* [ ] {0%} Choix de l’électronique
* [ ] {0%} Fabrication du 2ième prototype


## Les dossiers

Le dossier **01_Système** contient tous les éléments architecturant, la spécification générale du produit et toutes les informations qui consernent potentiellement tous les métiers

Les dossiers **02_ à 04_** servent à la conception du produit. Il contient toute l’étude et toutes les sources. Nous sommes organisés par métier. Ici on trouve la mécanique l’électronique, le logiciel, mais on pourrait en imaginer d’autres selon la nature du projet.

Le dossier **05_Fabrication** est destiné à ceux qui veulent  simplement fabriquer le produit sans trop réfléchir au détail de la conception. Ce dossier est un peu l’équivalent de la rubrique «Téléchargement» pour un projet de type logiciel libre. Mais évidement, un objet libre ne se fabrique pas aussi facilement qu’un simple téléchargement. La philosophie de ce dossier est d’expliquer le plus simplement possible comment fabriquer l’objet.


## Licence

La licence attribuée est différente selon le métier considéré. Aussi, il y a une licence différente dans chaque dossier selon la répartition suivante :

| Dossier         | Licence (SPDX identifier) |
| --------------- | ------------------------- |
| 01_Système      | CC-BY-SA-4.0-or-later     |
| 02_Mécanique    | CERN-OHL-S-2.0-or-later   |
| 03_Électronique | CERN-OHL-S-2.0-or-later   |
| 04_Logiciel     | GNU GPL-3.0-or-later      |
| 05_Fabrication  | CC-BY-SA-4.0-or-later     |

Voir <https://spdx.org/licenses/>


## À Propos de l’Auteur

__Lilian Tribouilloy :__

* __Formation :__ Ingénieur en électronique, diplômé de l’[ENSEA](https://www.ensea.fr/fr) en 2004.
* __Métier :__ Concepteur électronique spécialisé dans les radiofréquences et la CEM (Compatibilité ÉlectroMagnétique).
* __Exemples de produit conçu dans le cadre professionnel :__ Émetteur et Réémetteur pour la télévision numérique ; Amplificateur de puissance classe AB pour une modulation OFDM ; Calculateur entrées/sorties pour camion ; Tableau de bord pour véhicule spéciaux ; Boîtier de télématique pour la gestion de flotte.
* __Objet Libre conçu :__ [ToucheLibre](http://touchelibre.fr/), un clavier d’ordinateur ergonomique en bois. Alliant l’esthétique à l’utile, il s’inscrit dans des valeurs de liberté, d’écologie et de santé.


