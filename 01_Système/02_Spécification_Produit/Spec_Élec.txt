Soucoupe en 8
Ajout de fonctions électroniques

1/ Temps:
	- minuterie / chronométre, pour la bonne infusion
	- heure / alarme, pour te rappeller l’heure de la pause

(Cela pourrait être fait par une application dans le nano PC.)
(On peut récupérer un chronométre et trouver une solution pour l’intégrer.)


2/ Température:
	- mesure de la température de l’eau
	- dit si c’est OK pour l’infusion en fonction du type de thé

(Peut‑on mettre un thermométre laser à travers le verre côté tasse ?)


ATTENTION
Il faut que ce soit étanche. On doit pouvoir laver la soucoupe sans se soucier de l’électronique.
¿ Spec : Immersion à 50 cm ?

=> Affichage sous le verre côté infusion / patisserie
=> Touche sensitive (est‑ce qu’une touche sensitive peut être aussi magnétique ?)

